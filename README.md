# 公司人员管理系统前后台

## 使用技术
- koa2 + vue3 + element-plus


## 安装教程
```shell
# 后台
npm i #安装依赖
npm run start # 本地运行
npm run prd # 上线

#前端
yarn # 安装依赖
yarn dev # 本地运行
yarn build # 打包

```
