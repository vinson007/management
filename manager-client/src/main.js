import { createApp } from 'vue'
// import './style.css'
import App from './App.vue'
import router from './router'
import Element from 'element-plus'
import 'element-plus/dist/index.css'
import request from './utils/request'
import storage from './utils/storage'
import store from './store'

const app = createApp(App)

// 定义全局 $request
app.config.globalProperties.$request = request
// 定义全局 $storage
app.config.globalProperties.$storage = storage

app.use(router)
.use(store)
.use(Element)
.mount('#app')
