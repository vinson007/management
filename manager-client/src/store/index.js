/**
 * vuex状态管理
 */
import { createStore } from "vuex";
import storage from '@/utils/storage'
import mutations from './mutations'

const state = {
    userInfo: '' || storage.getItem('userInfo') // 获取用户信息
}

export default createStore({
    state,
    mutations
})