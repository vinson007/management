/**
 * 
 * 环境配置封装
 */

const env = import.meta.env.MODE || 'prod'

const envConfig = {
    dev: {
        baseApi: '/api',
        mockApi: 'https://www.fastmock.site/mock/a69f807ecbe0646b45f5f2cb17ceb0a0/api/'
    },
    test: {
        baseApi: '/test.futurefe.com/api',
        mockApi: 'https://www.fastmock.site/mock/a69f807ecbe0646b45f5f2cb17ceb0a0/api/'
    },
    prod: {
        baseApi: '//futurefe.com/api',
        mockApi: ''
    }
}

export default {
    env,
    mock: false,
    namespace: 'manager',
    ...envConfig[env]
}