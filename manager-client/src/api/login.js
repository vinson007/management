import request from '../utils/request'

export function login(userName, userPwd) {
    return request({
        method: 'post',
        url: '/users/login',
        data: {
            userName,
            userPwd
        }
    })
}