import { createRouter, createWebHashHistory } from 'vue-router'

import Home from '@/views/Home'

const routes = [
    {
        name: 'home',
        path: '/',
        meta: {
            title: '首页'
        },
        redirect: '/welcome',
        component: Home,
        children: [
            {
                name: 'welcome',
                path: '/welcome',
                meta: {
                    title: '欢迎页'
                },
                component: () => import('@/views/Welcome')
            }
        ]
    },
    {
        name: 'login',
        path: '/login',
        meta: {
            title: '登录页'
        },
        component: () => import('@/views/Login')
    }
]

const router = createRouter({
    history: createWebHashHistory(),
    routes
})

export default router