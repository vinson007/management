const Koa = require('koa')
const app = new Koa()
const views = require('koa-views')
const json = require('koa-json')
const onerror = require('koa-onerror')
const bodyparser = require('koa-bodyparser')
// const logger = require('koa-logger')
const log4js = require('./utils/log4j')

const router = require('koa-router')()
const users = require('./routes/users')


// error handler
onerror(app)

require('./config/db')

// app.use(() => {
//   ctx.body = 'hello'
// })
// middlewares
app.use(bodyparser({
  enableTypes:['json', 'form', 'text']
}))
app.use(json())
// app.use(logger())
app.use(require('koa-static')(__dirname + '/public'))

app.use(views(__dirname + '/views', {
  extension: 'pug'
}))

// logger
app.use(async (ctx, next) => {
  // const start = new Date()
  // await next()
  // const ms = new Date() - start
  // console.log(`${ctx.method} ${ctx.url} - ${ms}ms`)
  if(ctx.method.toLowerCase() === 'get') {
    log4js.info(`get params:${JSON.stringify(ctx.request.query)}`)
  } else if(ctx.method.toLowerCase() === 'post') {
    log4js.info(`post params:${JSON.stringify(ctx.request.body)}`)
  }
  await next()
})

// routes
router.prefix('/api')
router.use(users.routes(), users.allowedMethods())
app.use(router.routes(), router.allowedMethods())

// error-handling
app.on('error', (err, ctx) => {
  console.error('server error', err, ctx)
  log4js.error(err.message)
});

module.exports = app
