/**
 * 用户管理模块
 */
const router = require('koa-router')()
const User = require('../models/userSchema')
const util = require('../utils/util')

router.prefix('/users')

router.post('/login', async (ctx) => {
  const { userName, userPwd } = ctx.request.body
  if(!userName && !userPwd) {
    ctx.body = util.fail('参数错误', 10001)
    return
  }
  try {
    const res = await User.findOne({
      userName,
      userPwd
    })
    if(res) {
      ctx.body = util.success(res)
    } else {
      ctx.body = util.fail('账号或密码不正确', 40002)
    }
  } catch (err) {
    ctx.body = util.fail(err.msg)
  }
})

module.exports = router
