/**
 * 
 * 数据库链接
 */

const mongoose = require('mongoose')
const config = require('./index')
const log4js = require('../utils/log4j')

main()
.then(() => log4js.info('***数据库链接成功***'))
.catch(() =>log4js.error('***数据库链接失败***'))

async function main() {
  await mongoose.connect(config.URL);
  
  // use `await mongoose.connect('mongodb://user:password@localhost:27017/test');` if your database has auth enabled
}